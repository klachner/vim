# vim

things related to my favourite vim setup using pathogen to manage plugins

usage: clone repo (into ~/.vim), and run `clonePlugins.sh` 

if terminal complains about missing "exuberant ctags",
`apt-get/yum/brew/whatever install ctags`

### Plugins
- [nerdtree](https://github.com/preservim/nerdtree) to see file tree (press F4 in editor)
- [supertab](https://github.com/ervandew/supertab) for tab completion
- [vim-fugitive](https://github.com/tpope/vim-fugitive) for git commands inside vim
- [vim-sensible](https://github.com/tpope/vim-sensible) standard vim stuff
- [vimtex](https://github.com/lervag/vimtex/) for latex, use with skim; to compile project type `\ll` in editor


### How to install the newest version of vim on a machine without sudo rights: 
`$ git clone https://github.com/vim/vim.git .vim`

`$ cd vim/src`

`$ ./configure` or, if path in Makefile not default vim, ./configure --prefix=/somewhere/else/than/usr/local`

`$ make`

`$ make install` (if need be, fix path to dir in Makefile)

then, make sure it is used instead of standard vim: in .bashrc, (at the very end), export
export PATH=„/path/to/where/vim/was/cloned/.vim/src:$PATH“
(don’t forget to source .bashrc)

then clone this repo (into temporary dir) and move content from that dir into .vim. From there, like above `. clonePlugins.sh`

if issue with exuberant ctags remains, and ctags can not be installed, remove the line that clones taglist from clonePlugins.sh
