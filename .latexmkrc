$pdf_previewer = 'open -a Skim';
$pdflatex = 'pdflatex --shell-escape %O %S -synctex=1 -interaction=nonstopmode';
@generated_exts = (@generated_exts, 'synctex.gz');
