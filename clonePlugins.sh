#!/bin/sh

# TODO: should happen automatically since the corresponding 
# git repos are linked in the bundle directory

git clone https://github.com/preservim/nerdtree bundle/nerdtree
git clone https://github.com/ervandew/supertab bundle/supertab/
git clone https://github.com/tpope/vim-fugitive bundle/vim-fugitive
git clone https://github.com/tpope/vim-sensible bundle/vim-sensible/
git clone https://github.com/lervag/vimtex/ bundle/vimtex
